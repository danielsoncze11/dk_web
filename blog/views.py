from django.shortcuts import render
from .models import Post
from django.views.generic import CreateView, DetailView, UpdateView, DeleteView, ListView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

# Class to display homepage-posts
class PostListView(ListView):
    model = Post
    template_name = 'blog/home.html'
    context_object_name = 'posts'
    ordering = ['-date_posted']
    paginate_by = 2

# Function to display about me page
def about_me(request):
    # context = {'posts': Post.objects.all()}
    return render(request, 'blog/about_me.html')

# Function to display my work page
def my_work(request):
    # context = {'posts': Post.objects.all()}
    return render(request, 'blog/my_work.html')

# Class to display form for creating blog posts
class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'content']

    # function to add an author to create post
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

# Class to display the detail of the each post
class PostDetailView(DetailView):
    model = Post

# Class to display the form to update the post
class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    # function to check that the current user is the author of the respective post
    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False

# Class to display deletion of the blog post
class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/'

    # function to check that the current user is the author of the respective post
    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False