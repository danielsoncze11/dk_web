from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.models import User

# Model of the blog posts. Contains the fields of the model
class Post(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    # function to get self url
    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk': self.pk})

    # function to display title if call self
    def __str__(self):
        return self.title
