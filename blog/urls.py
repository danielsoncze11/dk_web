from . import views
from django.urls import path
from .views import (
    PostCreateView,
    PostDetailView,
    PostUpdateView,
    PostDeleteView,
    PostListView,
)

urlpatterns = [
    path('', PostListView.as_view(), name='blog-home'),
    path('aboutme/', views.about_me, name='blog-about_me'),
    path('mywork/', views.my_work, name='blog-my_work'),
    path('post/new/', PostCreateView.as_view(), name='post-create'),
    path('post/<int:pk>/', PostDetailView.as_view(), name='post-detail'),
    path('post/<int:pk>/update/', PostUpdateView.as_view(), name='post-update'),
    path('post/<int:pk>/delete/', PostDeleteView.as_view(), name='post-delete'),
]